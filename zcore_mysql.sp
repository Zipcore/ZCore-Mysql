#define PLUGIN_AUTHOR ".#Zipcore"
#define PLUGIN_NAME "Z-Core: MySQL"
#define PLUGIN_VERSION "1.4.1"
#define PLUGIN_DESCRIPTION "Simple database connection manager"
#define PLUGIN_URL "zipcore.net"

#include <sourcemod>
#include <sdktools>

#include <zcore/zcore_mysql>

public Plugin myinfo = 
{
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
}

//bool g_LateLoaded;

/* Database */
Handle g_aSQL = null;
Handle g_aSQLFailCounter = null;
Handle g_aSQLNames = null;

/* Forwards */
Handle g_OnSqlConnected;
Handle g_OnSqlError;

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	RegPluginLibrary("zcore-mysql");
	
	CreateNative("ZCore_Mysql_RequestDatabase", 	Native_ZCore_Mysql_RequestDatabase);
	CreateNative("ZCore_Mysql_GetConnection",	Native_ZCore_Mysql_GetConnection);
	
	g_OnSqlConnected = CreateGlobalForward("ZCore_Mysql_OnDatabaseConnected", ET_Event, Param_Cell, Param_String, Param_Cell);
	g_OnSqlError = CreateGlobalForward("ZCore_Mysql_OnDatabaseError", ET_Event, Param_Cell, Param_String, Param_String);
	
	g_aSQL = CreateArray(1);
	g_aSQLFailCounter = CreateArray(1);
	g_aSQLNames = CreateArray(64);
	
	return APLRes_Success;
}

public void OnPluginStart()
{
	CreateConVar("zcore_mysql_version", PLUGIN_VERSION, "Z-Core: MySQL version", FCVAR_PLUGIN|FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);
}

public int Native_ZCore_Mysql_RequestDatabase(Handle plugin, int numParams)
{
	char name[64];
	GetNativeString(1, name, 64);
	
	if(StrEqual("", name))
		return -1;
	
	for (int i = 0; i < GetArraySize(g_aSQL); i++)
	{
		char name2[64];
		GetArrayString(g_aSQLNames, i, name2, 64);
	
		if(!StrEqual(name, name2, false))
			continue;
			
		return i;
	}
	
	int index = PushArrayCell(g_aSQL, 0);
	PushArrayCell(g_aSQLFailCounter, 0);
	PushArrayString(g_aSQLNames, name);
	
	LogMessage("### Z-Core: MySQL ### New Database: %s (id:%d)", name, index);
	ConnectSQL(index);
	
	return index;
}


void ConnectSQL(int index)
{
	char name[64];
	GetArrayString(g_aSQLNames, index, name, 64);
	
	if(view_as<Handle>(GetArrayCell(g_aSQL, index)) != null)
	{
		LogError("### Z-Core: MySQL ### Already connected (id:%d)", name, index);
		return;
	}
	
	if (SQL_CheckConfig(name))
	{
		DataPack pack = new DataPack();
		pack.WriteCell(index);
		pack.WriteString(name);
		SQL_TConnect(ConnectSQLCallback, name, pack);
	}
	else LogError("### Z-Core: MySQL ### PLUGIN STOPPED - Reason: No config entry found for \"%s\" in databases.cfg - Check your databases.cfg and restart this server to fix this.", index);
}

public void ConnectSQLCallback(Handle owner, Handle hndl, const char[] error, any pack)
{
	ResetPack(pack);
	int index = ReadPackCell(pack);
	char name[64];
	ReadPackString(pack, name, sizeof(name));
	
	if (hndl == null || strlen(error) > 2)
	{
		int failcount = GetArrayCell(g_aSQLFailCounter, index);
		SetArrayCell(g_aSQLFailCounter, index, failcount+1);
		
		LogError("### Z-Core: MySQL ### No connection possible for \"%s\" in databases.cfg - Check your databases.cfg and restart this server to fix this. Try #%d, Error:%", name, failcount, error);
		
		PushSQLError(index, error);
		
		if(failcount > 10) 
		{
			LogMessage("### Z-Core: MySQL ### Reconnect \"%s\" in 30s", name);
			CreateTimer(30.0, Timer_ReConnect);
		}
		else if(failcount > 5) 
		{
			LogMessage("### Z-Core: MySQL ### Reconnect \"%s\" in 5s", name);
			CreateTimer(5.0, Timer_ReConnect);
		}
		else if(failcount > 1) 
		{
			LogMessage("### Z-Core: MySQL ### Reconnect \"%s\" in 1s", name);
			CreateTimer(1.0, Timer_ReConnect);
		}
		else 
		{
			LogMessage("### Z-Core: MySQL ### Reconnect \"%s\" now", name);
			CreateTimer(0.0, Timer_ReConnect);
		}
	
		return;
	}
	
	SetArrayCell(g_aSQLFailCounter, index, 1);
	SetArrayCell(g_aSQL, index, CloneHandle(hndl));
	
	SQL_SetCharset(GetArrayCell(g_aSQL, index), "utf8");
	
	Call_StartForward(g_OnSqlConnected);
	Call_PushCell(index);
	Call_PushString(name);
	Call_PushCell(GetArrayCell(g_aSQL, index));
	Call_Finish();
}

void PushSQLError(int index, const char[] error)
{
	char name[64];
	GetArrayString(g_aSQLNames, index, name, 64);
	LogError("### Z-Core: MySQL ### Connection failed for \"%s\". Reason: %s", name, error);
	
	Call_StartForward(g_OnSqlError);
	Call_PushCell(index);
	Call_PushString(name);
	Call_PushString(error);
	Call_Finish();
}

public Action Timer_ReConnect(Handle timer, any data)
{
	ConnectSQL(data);
	return Plugin_Stop;
}

public int Native_ZCore_Mysql_GetConnection(Handle plugin, int numParams)
{
	return GetArrayCell(g_aSQL, GetNativeCell(1));
}